﻿using UnityEngine;

namespace Scenes.Maps.Scripts
{
    public class PlayAmbientSound : MonoBehaviour
    {
        [Header("Wwise")] 
        public AK.Wwise.Event ambientSound;

        private void Awake()
        {
            ambientSound.Post(gameObject);
        }

        private void OnDestroy()
        {
            AkSoundEngine.StopAll();
        }
    }
}
