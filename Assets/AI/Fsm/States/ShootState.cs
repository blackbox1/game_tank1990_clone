﻿using GamePlay.Tanks.Scripts;
using UnityEngine;

namespace AI.Fsm.States
{
    public class ShootState : AbstractState
    {
        private TankShootComponent _shootComponent;
        
        public override void Enter(StateMachine stateMachine, GameObject owner)
        {
            base.Enter(stateMachine, owner);
            
            _shootComponent = owner.GetComponent<TankShootComponent>();
            
            Debug.Log("Entering ShootState");
        }

        public override void Update(double deltaTime)
        {
            Debug.Log("Shoot!");
            
            _shootComponent.Shoot();
            
            StateMachine.ChangeState(new SearchForState());
        }

        public override void Exit()
        {
        }
    }
}