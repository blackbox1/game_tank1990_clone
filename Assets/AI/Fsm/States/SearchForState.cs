﻿using System.Collections.Generic;
using Core.ObjectType;
using GamePlay.Tanks.Scripts;
using UnityEngine;

namespace AI.Fsm.States
{
    //TODO: A* Path finding sometimes include walls -> shoot them
    //TODO: Different behaviour based on tank (chase and shoot, search for base, search for player, shoot and run)
    //TODO: Tanks with Upgrades -> probability that the ai makes it hard for the player to catch it (flee if player is near)
    //TODO: Probability that the ai defends against bullets
    public class SearchForState : AbstractState
    {
        private System.Random _random;
        
        private TankMovementComponent _movementComponent;
        
        private double _timeElapsedDirectionChange;
        private double _timeElapsedLineOfSightCheck;
        
        private double _directionChangeInterval;
        private double _lineOfSightCheckInterval;
        
        private List<Vector3> _possibleDirections;
        private Vector3 _currentDirection;
        
        public override void Enter(StateMachine stateMachine, GameObject owner)
        {
            base.Enter(stateMachine, owner);
            
            Debug.Log("Entering SearchFor State");

            _movementComponent = owner.GetComponent<TankMovementComponent>();
            
            _random = new System.Random();
            
            _directionChangeInterval = _random.NextDouble() * 2.0f + 1.0f;
            _lineOfSightCheckInterval = _random.NextDouble() * 1.0f + 1.0f;

            _timeElapsedDirectionChange = _directionChangeInterval;
            _timeElapsedLineOfSightCheck = 0.0f;
            
            _possibleDirections = new List<Vector3>
            {
                new Vector3(1.0f, 0.0f), 
                new Vector3(0.0f, 1.0f), 
                new Vector3(-1.0f, 0.0f), 
                new Vector3(0.0f, -1.0f)
            };
        }

        public override void Update(double deltaTime)
        {
            Debug.Log("Search!");
            
            _timeElapsedDirectionChange += deltaTime;
            _timeElapsedLineOfSightCheck += deltaTime;

            if (_timeElapsedDirectionChange >= _directionChangeInterval)
            {
                _timeElapsedDirectionChange = 0.0f;
                _currentDirection = GenerateRandomDirection();
            }
            
            if (_timeElapsedLineOfSightCheck >= _lineOfSightCheckInterval)
            {
                Debug.Log("LineOfSightCheck!");
                
                _timeElapsedLineOfSightCheck = 0.0f;

                var position = Owner.transform.position;
                var hit = Physics2D.Raycast(position, _currentDirection);
                Debug.DrawRay(position, _currentDirection, Color.magenta);
                
                if (hit.collider.gameObject.CompareTag("Damageable") )
                {
                    Debug.Log("Found Target: " + hit.collider.tag);
                    
                    var objectTypeDataComponent = hit.collider.gameObject.GetComponent<ObjectTypeDataComponent>();

                    if (objectTypeDataComponent != null &&
                        objectTypeDataComponent.objectTypeData.objectType != ObjectType.Upgrade)
                    {
                        StateMachine.ChangeState(new ShootState());
                    }
                }
            }
            
            _movementComponent.Move(_currentDirection);
        }

        public override void Exit()
        {
        }
        
        private Vector3 GenerateRandomDirection()
        {
            var randomDirectionIndex = _random.Next(0, _possibleDirections.Count);
            return _possibleDirections[randomDirectionIndex];
        }
    }
}