﻿using System;
using Core.ObjectType;
using GamePlay.Upgrades.Scripts;
using UnityEngine;

namespace Core.Spawning.Scripts
{
    [ExecuteInEditMode]
    public class SpawnPoint : MonoBehaviour
    {
        [Header("SpawnInfo")]
        public GameObject objectToSpawn;
        public int timeToSpawn;
        public bool isPlayer;
        public ObjectType.ObjectType objectType;
        public bool drops;

        private void Awake()
        {
            var objectTypeDataComponent = objectToSpawn.GetComponentInChildren<ObjectTypeDataComponent>(true);
            objectType = objectTypeDataComponent.objectTypeData.objectType;
        }

        private void OnDrawGizmos()
        {
            switch (objectType)
            {
                case ObjectType.ObjectType.Tank:
                    Gizmos.color = Color.red;
                    
                    if(isPlayer)
                        Gizmos.color = Color.blue;
                    break;
                case ObjectType.ObjectType.Bunker:
                    Gizmos.color = Color.magenta;
                    break;
                case ObjectType.ObjectType.Wall:
                    Gizmos.color = Color.gray;
                    break;
                case ObjectType.ObjectType.Upgrade:
                    Gizmos.color = Color.cyan;
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
            
            Gizmos.DrawSphere(transform.position, 0.25f);
        }
    }
}
