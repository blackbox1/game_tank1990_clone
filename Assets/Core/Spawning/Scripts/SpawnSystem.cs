﻿using System;
using System.Collections.Generic;
using AI;
using Core.Health;
using Core.Input;
using Core.ObjectType;
using Core.RefVariables;
using GamePlay.Tanks.Scripts;
using GamePlay.Upgrades.Scripts;
using UnityEngine;
using UnityEngine.Tilemaps;

namespace Core.Spawning.Scripts
{
    public class SpawnSystem : MonoBehaviour
    {
        private const string WalkableTag = "Walkable";
        private const string SpawnPointTag = "Spawn";
        [HideInInspector] public List<SpawnPoint> spawnPoints;
        private float _elapsedTime;
        
        [Header("Upgrades")]
        public List<Upgrade> upgrades;
        private Tilemap _walkableTileMap;
        private List<Vector3> _tileWorldLocations;

        [Header("Tracking")]
        public GameObjectsVariable enemies;
        public GameObjectVariable player;
        public RelatedGameObjectsVariable bunker;
        public SpawnInfoVariable spawnInfo;
        public GameObjectVariable upgrade;

        [Header("Events")] 
        public GameEvent.GameEvent updateSpawnInfoEvent;
        public GameEvent.GameEvent spawnUpgradeEvent;
        
        private SpawnPoint _nextSpawn;
        private SpawnPoint _playerSpawn;
        
        private void Awake()    
        {
            var tileMapGo = GameObject.FindGameObjectWithTag(WalkableTag);
            if (tileMapGo != null)
            {
                _walkableTileMap = tileMapGo.GetComponent<Tilemap>();
                
                _tileWorldLocations = new List<Vector3>();

                foreach (var pos in _walkableTileMap.cellBounds.allPositionsWithin)
                {   
                    var localPlace = new Vector3Int(pos.x, pos.y, pos.z);
                    var place = _walkableTileMap.CellToWorld(localPlace);
                
                    if (_walkableTileMap.HasTile(localPlace))
                        _tileWorldLocations.Add(place);
                }
            }
            
            var gameObjects = GameObject.FindGameObjectsWithTag(SpawnPointTag);

            if (gameObjects.Length <= 0) return;
            
            var totalEnemies = 0;
            
            foreach (var go in gameObjects)
            {
                var spawnPoint = go.GetComponent<SpawnPoint>();
                var objectTypeDataComponent =
                    spawnPoint.objectToSpawn.GetComponentInChildren<ObjectTypeDataComponent>(true);
                
                if (!spawnPoint.isPlayer && objectTypeDataComponent.objectTypeData.objectType == ObjectType.ObjectType.Tank)
                    totalEnemies++;

                if(spawnPoint != null)
                    spawnPoints.Add(spawnPoint);
            }

            enemies.destroyedGameObjects = 0;
            enemies.totalGameObjects = totalEnemies;

            spawnPoints.Sort(delegate (SpawnPoint x, SpawnPoint y)
            {
                if (x.timeToSpawn > y.timeToSpawn)
                    return 1;
                if (x.timeToSpawn < y.timeToSpawn)
                    return -1;

                return 0;
            });

            NextSpawnPoint();
        }

        private void NextSpawnPoint()
        {
            if (spawnPoints.Count <= 0)
            {
                _nextSpawn = null;
                spawnInfo.image = null;
                spawnInfo.label = "No reinforcements!";
                updateSpawnInfoEvent.Raise();
                return;
            }

            _nextSpawn = spawnPoints[0];
            
            if (_nextSpawn.isPlayer)
            {
                _playerSpawn = _nextSpawn;
            }
            
            spawnPoints.RemoveAt(0);

            if (_nextSpawn.objectType != ObjectType.ObjectType.Tank) return;
            
            var tdc = _nextSpawn.objectToSpawn.GetComponentInChildren<TankDataComponent>();
            spawnInfo.image = tdc.tankData.icon;
            spawnInfo.label = tdc.tankData.label;
            updateSpawnInfoEvent.Raise();
        }

        private void Update()
        {
            _elapsedTime += Time.deltaTime;
            
            if (_nextSpawn == null || !(_nextSpawn.timeToSpawn <= _elapsedTime)) return;
            
            var go = Instantiate(_nextSpawn.objectToSpawn, _nextSpawn.gameObject.transform);
            
            switch (_nextSpawn.objectType)
            {
                case ObjectType.ObjectType.Tank:
                    if (_nextSpawn.isPlayer)
                    {
                        go.AddComponent<InputComponent>();
                        player.gameObject = go;
                    }
                    else
                    {
                        go.AddComponent<AiComponent>();
                        enemies.activeGameObjects.Add(go);

                        var cd = go.GetComponent<CanDrop>();
                        if (_nextSpawn.drops)
                        {
                            var hc = go.GetComponent<HealthComponent>();
                            hc.onDeathEvent = spawnUpgradeEvent;

                            cd.enabled = true;
                        }
                        else
                            cd.enabled = false;
                    }
                    break;
                case ObjectType.ObjectType.Bunker:
                    bunker.gameObject = go;
                    break;
                case ObjectType.ObjectType.Wall:
                    break;
                case ObjectType.ObjectType.Upgrade:
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            NextSpawnPoint();
        }

        public void SpawnPlayer()
        {
            if (_playerSpawn == null) return;
            
            var go = Instantiate(_playerSpawn.objectToSpawn, _playerSpawn.gameObject.transform);
            go.AddComponent<InputComponent>();
            player.gameObject = go;
        }

        public void SpawnUpgrade()
        {
            if (upgrades.Count <= 0) return;
            
            if (upgrade.gameObject != null)
                Destroy(upgrade.gameObject);
            
            var randomIndex = UnityEngine.Random.Range (0, _tileWorldLocations.Count);
            var location = _tileWorldLocations[randomIndex];
            
            upgrade.gameObject = Instantiate(upgrades[0].gameObject, location, Quaternion.identity);
            
            upgrades.RemoveAt(0);
        }
    }
}
