﻿using System;
using GamePlay.Tanks.Scripts;
using UnityEngine;

namespace Core.Input
{
    public class InputComponent : MonoBehaviour
    {
        private InputMaster _inputMaster;
        private TankMovementComponent _movementComponent;
        private TankShootComponent _shootComponent;

        private void Awake()
        {
            _movementComponent = GetComponent<TankMovementComponent>();
            if (_movementComponent == null)
                Debug.LogWarning("MovementComponent is not set");

            _shootComponent = GetComponent<TankShootComponent>();
            if (_shootComponent == null)
                Debug.LogWarning("ShootComponent is not set");

            _inputMaster = new InputMaster();

            _inputMaster.Player.Shoot.performed += (_ => Shoot());
            //_inputMaster.Player.Movement.performed += (context => Move(context.ReadValue<Vector2>()));
        }

        private void Update()
        {
            Move(_inputMaster.Player.Movement.ReadValue<Vector2>());
        }

        private void Shoot()
        {
            if(_shootComponent != null)
                _shootComponent.Shoot();
        }

        private void Move(Vector2 direction)
        {
//            Debug.Log(direction);
            if(_movementComponent != null)
                _movementComponent.Move(direction);
        }

        private void OnEnable()
        {
            _inputMaster.Enable();
        }

        private void OnDisable()
        {
            _inputMaster.Disable();
        }
    }
}
