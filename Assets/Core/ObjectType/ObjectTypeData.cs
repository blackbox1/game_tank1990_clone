using UnityEngine;
using UnityEngine.Serialization;

namespace Core.ObjectType
{
    [CreateAssetMenu]
    public class ObjectTypeData : ScriptableObject
    {
        [FormerlySerializedAs("ObjectType")] public ObjectType objectType;
    }
}