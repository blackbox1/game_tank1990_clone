namespace Core.ObjectType
{
    public enum ObjectType {
        Tank,
        Bunker,
        Wall,
        Upgrade
    };
}