﻿using System;
using Core.RefVariables;
using UnityEngine;

namespace Core.Score
{
    public class AddsToScore : MonoBehaviour
    {
        [Header("Tracking")]
        public IntVariable playerScore;
        public GameEvent.GameEvent updateScoreEvent;
        
        [Header("Score")] 
        public int amount;

        private void Awake()
        {
            updateScoreEvent.Raise();
        }

        public void AddToScore()
        {
            if (playerScore == null) return;

            playerScore.value += amount;
            
            updateScoreEvent.Raise();
        }
    }
}
