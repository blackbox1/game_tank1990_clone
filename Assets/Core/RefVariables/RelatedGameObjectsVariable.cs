﻿using System.Collections.Generic;
using UnityEngine;

namespace Core.RefVariables
{
    [CreateAssetMenu]
    public class RelatedGameObjectsVariable : ScriptableObject
    {
        public GameObject gameObject;
        public List<GameObject> relatedGameObjects;
    }
}
