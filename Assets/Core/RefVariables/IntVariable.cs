﻿using UnityEngine;
using UnityEngine.Serialization;

namespace Core.RefVariables
{
    [CreateAssetMenu]
    public class IntVariable : ScriptableObject
    {
        public int value;
    }
}
