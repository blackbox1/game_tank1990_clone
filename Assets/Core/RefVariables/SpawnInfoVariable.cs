using UnityEngine;

namespace Core.RefVariables
{
    [CreateAssetMenu]
    public class SpawnInfoVariable : ScriptableObject
    {
        public string label;
        public Sprite image;
    }
}