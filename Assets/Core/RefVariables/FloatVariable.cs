﻿using UnityEngine;
using UnityEngine.Serialization;

namespace Core.RefVariables
{
    [CreateAssetMenu]
    public class FloatVariable : ScriptableObject
    {
        public float value;
    }
}
