﻿using UnityEngine;
using UnityEngine.Serialization;

namespace Core.RefVariables
{
    [CreateAssetMenu]
    public class BoolVariable : ScriptableObject
    {
        public bool value;
    }
}
