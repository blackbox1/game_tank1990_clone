﻿using System.Collections.Generic;
using UnityEngine;

namespace Core.RefVariables
{
    [CreateAssetMenu]
    public class GameObjectsVariable : ScriptableObject
    {
        public int totalGameObjects;
        public int destroyedGameObjects;
        public List<GameObject> activeGameObjects;
    }
}
