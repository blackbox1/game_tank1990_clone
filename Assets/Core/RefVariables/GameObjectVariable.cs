﻿using UnityEngine;
using UnityEngine.Serialization;

namespace Core.RefVariables
{
    [CreateAssetMenu]
    public class GameObjectVariable : ScriptableObject
    {
        public GameObject gameObject;
    }
}
