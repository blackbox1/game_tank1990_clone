﻿using Core.ObjectType;
using UnityEngine;
using UnityEngine.Serialization;

namespace Core.Health
{
    [CreateAssetMenu]
    public class HealthData : ObjectTypeData
    {
        public int health;
    }
}
