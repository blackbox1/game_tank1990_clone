﻿using System.Collections;
using System.Collections.Generic;
using Core.RefVariables;
using Core.Score;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Serialization;

namespace Core.Health
{
    [RequireComponent(typeof(HealthDataComponent))]
    public class HealthComponent : MonoBehaviour
    {
        private Animator _animator;
        private SpriteRenderer _spriteRenderer;
        private SpriteRenderer[] _childrenSpriteRenderers;
        private static readonly int Health = Animator.StringToHash("Health");
        
        [HideInInspector] public HealthDataComponent healthDataComponent;
        [HideInInspector] public int currentHealth;
        
        private bool _hasShield;
        private float _shieldDurationRemaining;
        private float _colorDurationRemaining;

        [Header("Appearance")] 
        public SpriteByHealth spriteByHealthComponent;

        [Header("Death")]
        public AK.Wwise.Event destroySound;
        public GameEvent.GameEvent onDeathEvent;
        public GameObject remainsToSpawn;

        [Header("Tracking")]
        public GameObjectsVariable enemies;
        public GameObjectsVariable remains;
        public GameObjectVariable player;
        public RelatedGameObjectsVariable bunker;

        private void Awake()
        {
            healthDataComponent = GetComponent<HealthDataComponent>();
            _animator = GetComponent<Animator>();
            _spriteRenderer = GetComponent<SpriteRenderer>();

            _childrenSpriteRenderers = GetComponentsInChildren<SpriteRenderer>();
            
            gameObject.tag = "Damageable";
            currentHealth = healthDataComponent.healthData.health;
            
            if (_animator == null) return;
            
            _animator.logWarnings = false;
            _animator.SetInteger(Health, currentHealth);
        }

        private void Update()
        {
            _shieldDurationRemaining -= Time.deltaTime;
            _colorDurationRemaining -= Time.deltaTime;
            
            if (_colorDurationRemaining <= 0.0f)
                ChangeColorForDuration(_colorDurationRemaining, Color.white);

            if (!(_shieldDurationRemaining <= 0.0f)) return;
            
            _hasShield = false;
            _shieldDurationRemaining = 0.0f;
        }

        public void ModifyHealth(int amount, bool byPlayer)
        {
            currentHealth += amount;

            if(currentHealth > healthDataComponent.healthData.health)
                currentHealth = healthDataComponent.healthData.health;

            if (spriteByHealthComponent != null)
                spriteByHealthComponent.UpdateSpriteByHealth();
            
            if(_animator != null)
                _animator.SetInteger(Health, currentHealth);

            if (currentHealth > 0 || _hasShield) return;

            if (byPlayer)
            {
                var scoreComponent = gameObject.GetComponent<AddsToScore>();

                if (scoreComponent != null)
                {
                    scoreComponent.AddToScore();
                }
            }
            
            if (player != null && player.gameObject == gameObject)
                player.gameObject = null; 
            else if (bunker != null && bunker.gameObject == gameObject)
                bunker.gameObject = null;
            else if (enemies.activeGameObjects.Contains(gameObject))
            {
                enemies.activeGameObjects.Remove(gameObject);
                enemies.destroyedGameObjects++;
            }

            var delay = 0.0f;
            
            if (_animator != null)
                delay = _animator.GetCurrentAnimatorStateInfo(0).length;
            
            if (remainsToSpawn != null)
                StartCoroutine(WaitAndSpawnRemains(remainsToSpawn, gameObject, remains, delay));
            else 
                Destroy(gameObject, delay);
        }

        public void ApplyShield(float duration)
        {
            _hasShield = true;
            _shieldDurationRemaining = duration;
        }

        public void ChangeColorForDuration(float duration, Color color)
        {
            _colorDurationRemaining = duration;
            
            if(_spriteRenderer != null) _spriteRenderer.color = color;
            
            foreach (var spriteRenderer in _childrenSpriteRenderers)
            {
                spriteRenderer.color = color;
            }
        }
        
        private static IEnumerator WaitAndSpawnRemains(GameObject remainsToSpawn, GameObject parent, GameObjectsVariable remainsTracker, float waitForSeconds)
        {
            yield return new WaitForSeconds(waitForSeconds);

            var go = Instantiate(remainsToSpawn, parent.transform.position, Quaternion.identity);

            if (remainsTracker == null) yield break;
            
            remainsTracker.activeGameObjects.Add(go);
            remainsTracker.totalGameObjects++;
            
            Destroy(parent);
        }

        private void OnDestroy()
        {
            if (onDeathEvent != null) onDeathEvent.Raise();

            destroySound.Post(gameObject);
        }
    }
}
