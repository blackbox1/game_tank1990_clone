﻿using Core.ObjectType;
using UnityEngine.Serialization;

namespace Core.Health
{
    public class HealthDataComponent : ObjectTypeDataComponent
    {
        [FormerlySerializedAs("HealthData")] public HealthData healthData;
    }
}
