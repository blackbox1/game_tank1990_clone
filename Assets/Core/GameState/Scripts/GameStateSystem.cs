﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Remoting.Messaging;
using Core.RefVariables;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Serialization;

namespace Core.GameState.Scripts
{
    public class GameStateSystem : MonoBehaviour
    {
        [Header("Huds")] 
        public GameObject victoryPanel;
        public GameObject defeatPanel;

        [Header("Transition")] 
        public float transitionDelay;
        public string menuSceneName;
        public string nextLevelSceneName;

        [Header("Pausing")] 
        public float timeScaleFactor;
        
        [Header("Tracking")]
        public GameObjectsVariable enemies;
        public GameObjectVariable player;
        public GameObjectVariable upgrade;
        public RelatedGameObjectsVariable bunker;
        public GameObjectsVariable remains;
        public IntVariable score;
        public IntVariable lives;
        
        [Header("Events")]
        public GameEvent.GameEvent spawnPlayerEvent;

        public GameEvent.GameEvent healthUpdateEvent;
        
        private void Awake()
        {
            Time.timeScale = 1.0f;

            CleanUp();
        }

        public void CheckState()
        {
            if (player.gameObject == null && lives.value > 0)
            {
                lives.value--;
                spawnPlayerEvent.Raise();
                healthUpdateEvent.Raise();
            }

            if (lives.value <= 0 || bunker.gameObject == null || !bunker.gameObject.activeSelf)
            {
                Time.timeScale = timeScaleFactor;
                
                if(defeatPanel != null)
                    defeatPanel.SetActive(true);
                
                StartCoroutine(WaitAndLoadScene(menuSceneName, transitionDelay * timeScaleFactor));
            }

            if (enemies.destroyedGameObjects < enemies.totalGameObjects) return;
            
            Time.timeScale = timeScaleFactor;
            victoryPanel.SetActive(true);
            
            StartCoroutine(WaitAndLoadScene(nextLevelSceneName, transitionDelay * timeScaleFactor));
        }

        private static IEnumerator WaitAndLoadScene(string sceneName, float waitForSeconds)
        {
            yield return new WaitForSeconds(waitForSeconds);

            SceneManager.LoadScene(sceneName);
        }

        private void CleanUp()
        {
            lives.value = 3;
            
            score.value = 0;
            
            if (player.gameObject != null)
            {
                Destroy(player.gameObject);
            }

            if (bunker.gameObject != null)
            {
                Destroy(bunker.gameObject);
            }

            bunker.relatedGameObjects.Clear();
            
            enemies.totalGameObjects = 0;
            enemies.destroyedGameObjects = 0;
            enemies.activeGameObjects.Clear();
            remains.totalGameObjects = 0;
            remains.destroyedGameObjects = 0;
            remains.activeGameObjects.Clear();

            if (upgrade.gameObject != null)
            {
                Destroy(upgrade.gameObject);
            }
            
        }
        
        private void OnDestroy()
        {
            CleanUp();
        }
    }
}
