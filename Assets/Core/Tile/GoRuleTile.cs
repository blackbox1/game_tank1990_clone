﻿using Core.RefVariables;
using UnityEngine;
using UnityEngine.Tilemaps;

namespace Core.Tile
{
    [CreateAssetMenu(fileName = "New GO Rule Tile", menuName = "Tiles/GO Rule Tile")]
    public class GoRuleTile : RuleTile
    {
        [Header("GoRuleTile")] 
        public GameObject instance;
        public Vector3Int instanceLocation = new Vector3Int(0,0,0);
        public RelatedGameObjectsVariable relatedGameObject;

        public override bool StartUp(Vector3Int location, ITilemap tilemap, GameObject instantiatedGameObject)
        {
            instance = instantiatedGameObject;
            instanceLocation = location;
            
            if(relatedGameObject != null)
                relatedGameObject.relatedGameObjects.Add(instance);
            
            return base.StartUp(location, tilemap, instantiatedGameObject);
        }
    }
}
