﻿using System;
using UnityEngine;

namespace GamePlay.Tanks.Scripts
{
    public class CanDrop : MonoBehaviour
    {
        [Header("Appearance")]
        public GameObject dropEffectGameObject;
        private ParticleSystem _dropEffect;
        private GameObject _instantiatedEffect;

        public void Start()
        {
            if (dropEffectGameObject == null) return;

            _instantiatedEffect = Instantiate(dropEffectGameObject, transform.position, Quaternion.identity);
            _instantiatedEffect.transform.parent = transform;
            _dropEffect = _instantiatedEffect.GetComponent<ParticleSystem>();
            
            if (enabled)
                _dropEffect.Play();
            else 
                _dropEffect.Stop();
        }

        public void OnEnable()
        {
            if (_dropEffect == null) return;
            
            _dropEffect.Play();
        }

        public void OnDisable()
        {
            if (_dropEffect == null) return;
            
            _dropEffect.Stop();
        }

        public void OnDestroy()
        {
            Destroy(_instantiatedEffect);
        }
    }
}
