﻿using System;
using UnityEngine;

namespace GamePlay.Tanks.Scripts
{
    [RequireComponent(typeof(Rigidbody2D))]
    [RequireComponent(typeof(TankMovementComponent))]
    public class TankAnimationController : MonoBehaviour
    {
        public Animator bodyAnimator;
        public Animator turretAnimator;

        private Rigidbody2D _rigidBody;
        private TankMovementComponent _movementComponent;
        
        private static readonly int IsMoving = Animator.StringToHash("IsMoving");
        private static readonly int IsOrientedHorizontal = Animator.StringToHash("IsOrientedHorizontal");
        private static readonly int IsLookingUp = Animator.StringToHash("IsLookingUp");
        private static readonly int IsLookingDown = Animator.StringToHash("IsLookingDown");
        private static readonly int IsLookingLeft = Animator.StringToHash("IsLookingLeft");
        private static readonly int IsLookingRight = Animator.StringToHash("IsLookingRight");
        private static readonly int VerticalValue = Animator.StringToHash("VerticalValue");
        private static readonly int HorizontalValue = Animator.StringToHash("HorizontalValue");

        private void Awake()
        {
            _movementComponent = GetComponent<TankMovementComponent>();
            _rigidBody = GetComponent<Rigidbody2D>();

            if (bodyAnimator == null)
                Debug.LogWarning("Body Animator not set");

            if (turretAnimator == null)
                Debug.LogWarning("Body Animator not set");
        }

        private void UpdateStates()
        {
            if(bodyAnimator != null)
            {
                bodyAnimator.SetBool(IsMoving, _rigidBody.velocity.sqrMagnitude > 0);
                bodyAnimator.SetBool(IsOrientedHorizontal, Math.Abs(_movementComponent.forwardDirection.x) > 0.1f);
                bodyAnimator.SetFloat(VerticalValue, _movementComponent.forwardDirection.y);
                bodyAnimator.SetFloat(HorizontalValue, _movementComponent.forwardDirection.x);
            }

            if (turretAnimator == null) return;
            
            turretAnimator.SetBool(IsLookingUp, _movementComponent.forwardDirection.y > 0.0f);
            turretAnimator.SetBool(IsLookingDown, _movementComponent.forwardDirection.y < 0.0f);
            turretAnimator.SetBool(IsLookingLeft, _movementComponent.forwardDirection.x < 0.0f);
            turretAnimator.SetBool(IsLookingRight, _movementComponent.forwardDirection.x > 0.0f);
        }

        private void Update()
        {
            UpdateStates();
        }
    }
}
