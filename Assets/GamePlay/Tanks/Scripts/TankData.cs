﻿using System.Collections.Generic;
using Core.Health;
using UnityEngine;

namespace GamePlay.Tanks.Scripts
{
    [CreateAssetMenu]
    public class TankData : HealthData
    {
        public int speed;
        public int shellSpeed;
        public int shellPower;
        public int shellLevel;
        public int activeShells;
        public GameObject shell;
        public Sprite icon;
        public string label; 
    }
}
