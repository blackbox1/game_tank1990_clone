﻿using System;
using Effects.Scripts;
using UnityEngine;

namespace GamePlay.Tanks.Scripts
{
    [RequireComponent(typeof(TankDataComponent))]
    [RequireComponent(typeof(Rigidbody2D))]
    public class TankMovementComponent : MonoBehaviour
    {
        [Header("Wwise")]
        public AK.Wwise.Event engine;

        [Header("Movement")]
        public int baseSpeedUnits = 75;

        [Header("Effects")] 
        public TrailEffect trailEffect;
        
        [HideInInspector] public Vector2 forwardDirection = new Vector2(0.0f, 1.0f);
        [HideInInspector] public int currentSpeed;
        
        private bool _isDeactivated;
        private float _deactivatedDuration;
        
        private TankDataComponent _tankDataComponent;
        private Rigidbody2D _rigidBody;

        private void Awake()
        {
            _tankDataComponent = GetComponent<TankDataComponent>();
            _rigidBody = GetComponent<Rigidbody2D>();

            currentSpeed = 0;

            engine.Post(gameObject);
        }

        public void Move(Vector2 direction)
        {
            if (_isDeactivated) return;
            
            if (Math.Abs(direction.x) > 0.001f || Math.Abs(direction.y) > 0.001f)
            {
                forwardDirection = CorrectInput(forwardDirection, direction);
                currentSpeed = _tankDataComponent.tankData.speed;

                if (trailEffect == null) return;
                trailEffect.forwardDirection = forwardDirection;
                
                trailEffect.SetEnabled(true);
            }
            else
            {
                currentSpeed = 0;
                
                if (trailEffect == null) return;
                trailEffect.SetEnabled(false);
            }
        }
        
        public void Deactivate(float duration)
        {
            _isDeactivated = true;
            _deactivatedDuration = duration;
        }

        private static Vector2 CorrectInput(Vector2 previousDirection, Vector2 direction)
        {
            if (Math.Abs(direction.x) > 0.001f && Math.Abs(direction.y) > 0.001f)
                return previousDirection.normalized;

            return direction.normalized;
        }
        
        private void Update()
        {
            _deactivatedDuration -= Time.deltaTime;
            
            if (!_isDeactivated || !(_deactivatedDuration <= 0.0f)) return;
            
            _isDeactivated = false;
            _deactivatedDuration = 0.0f;
        }

        private void FixedUpdate()
        {
            if (_isDeactivated) return;
            
            _rigidBody.velocity = forwardDirection * (baseSpeedUnits * currentSpeed * Time.deltaTime);
        }

        private void OnDestroy()
        {
            AkSoundEngine.StopAll(gameObject);
        }
    }
}
