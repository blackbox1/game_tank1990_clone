﻿using System;
using System.Collections.Generic;
using Core.Input;
using GamePlay.Projectiles.Scripts;
using UnityEngine;

namespace GamePlay.Tanks.Scripts
{
    [RequireComponent(typeof(TankDataComponent))]
    [RequireComponent(typeof(TankMovementComponent))]
    public class TankShootComponent : MonoBehaviour
    {
        [Header("Projectile")]
        public float offsetScale;
        public ParticleSystem fireEffect;

        [Header("Wwise")]
        public AK.Wwise.Event engine;

        private bool _isDeactivated;
        private float _deactivatedDuration;
        
        private TankDataComponent _tankDataComponent;
        private TankMovementComponent _movementComponent;
        private List<GameObject> _currentShells;
        private bool _isPlayer;
        private ParticleSystem _fireSystem;

        private void Awake()
        {
            _tankDataComponent = GetComponent<TankDataComponent>();
            _movementComponent = GetComponent<TankMovementComponent>();
            _currentShells = new List<GameObject>();
        }

        private void Start()
        {
            var inputComponent = GetComponent<InputComponent>();
            _isPlayer = inputComponent != null;
        }

        private void Update()
        {
            _deactivatedDuration -= Time.deltaTime;
            
            if (!_isDeactivated || !(_deactivatedDuration <= 0.0f)) return;
            
            _isDeactivated = false;
            _deactivatedDuration = 0.0f;
        }

        public void Shoot()
        {
            if (_isDeactivated) return;
            
            for (var index = _currentShells.Count-1; index >= 0;index--)
            {
                if(_currentShells[index] == null)
                    _currentShells.RemoveAt(index);
            }

            TankData td = null;
            if (_tankDataComponent.currentTankData == null)
                td = _tankDataComponent.tankData;
            else
                td = _tankDataComponent.currentTankData;

            if (_currentShells == null || _currentShells.Count >= td.activeShells) return;

            var angle = Mathf.Atan2(_movementComponent.forwardDirection.y, _movementComponent.forwardDirection.x) * Mathf.Rad2Deg;
            var position = transform.position;
            var projectilePosition = new Vector2(position.x, position.y) + _movementComponent.forwardDirection * offsetScale;
            var projectile = Instantiate(td.shell, projectilePosition, Quaternion.AngleAxis(angle, Vector3.forward));
            
            if (fireEffect != null)
            {
                if (_fireSystem == null)
                    _fireSystem = Instantiate(fireEffect, projectilePosition, Quaternion.identity);
                else
                {
                    _fireSystem.transform.position = projectilePosition;
                    _fireSystem.Play();
                }
            }
            
            InitializeProjectile(projectile, td);

            _currentShells.Add(projectile);

            engine.Post(gameObject);
        }

        public void Deactivate(float duration)
        {
            _isDeactivated = true;
            _deactivatedDuration = duration;
        }
        
        private void InitializeProjectile(GameObject projectile, TankData td)
        {
            var projectileComponent = projectile.GetComponent<ProjectileComponent>();
            projectileComponent.forwardDirection = _movementComponent.forwardDirection;
            projectileComponent.shellPower = td.shellPower;
            projectileComponent.shellSpeed = td.shellSpeed;
            projectileComponent.belongsToPlayer = _isPlayer;
        }

        private void OnDestroy()
        {
            Destroy(_fireSystem);
        }
    }
}
