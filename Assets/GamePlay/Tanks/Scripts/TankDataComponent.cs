﻿using Core.Health;
using UnityEngine;
using UnityEngine.Serialization;

namespace GamePlay.Tanks.Scripts
{
    public class TankDataComponent : HealthDataComponent
    {
        public TankData tankData;
        public TankData currentTankData;

        public void CreateInstancedTankData()
        {
            if (currentTankData != null) return;
            
            currentTankData = ScriptableObject.CreateInstance<TankData>();
            currentTankData.icon = tankData.icon;
            currentTankData.label = tankData.label;
            currentTankData.shell = tankData.shell;
            currentTankData.speed = tankData.speed;
            currentTankData.activeShells = tankData.activeShells;
            currentTankData.shellLevel = tankData.shellLevel;
            currentTankData.shellPower = tankData.shellPower;
            currentTankData.shellSpeed = tankData.shellSpeed;
        }
    }
}
