﻿using Core.Health;
using UnityEngine;

namespace GamePlay.Projectiles.Scripts
{
    [RequireComponent(typeof(Rigidbody2D))]
    public class ProjectileComponent : MonoBehaviour
    {
        [Header("Projectile")]
        public int baseSpeed = 200;

        [Header("Effects")] 
        public float effectDestructionDelay;
        public GameObject hitEffect;

        [HideInInspector] public Vector2 forwardDirection;
        [HideInInspector] public int shellPower;
        [HideInInspector] public int shellSpeed;
        [HideInInspector] public bool belongsToPlayer;

        private Rigidbody2D _rigidBody;

        private void Awake()
        {
            _rigidBody = GetComponent<Rigidbody2D>();
        }

        private void FixedUpdate()
        {
            _rigidBody.velocity = forwardDirection * (shellSpeed * baseSpeed * Time.deltaTime);
        }

        private void OnTriggerEnter2D(Collider2D collisionObject)
        {
            if (!collisionObject.gameObject.CompareTag("Damageable")) return;

            var effect = Instantiate(hitEffect, gameObject.transform.position, Quaternion.identity);
            Destroy(effect, effectDestructionDelay);

            Destroy(gameObject);
            var collisionObjectHc = collisionObject.gameObject.GetComponent<HealthComponent>();
            
            if (collisionObjectHc == null) return;
            
            collisionObjectHc.ModifyHealth(-shellPower, belongsToPlayer);
        }
    }
}
