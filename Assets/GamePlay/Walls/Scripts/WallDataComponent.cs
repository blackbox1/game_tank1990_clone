﻿using Core.Health;
using UnityEngine.Serialization;

namespace GamePlay.Walls.Scripts
{
    public class WallDataComponent : HealthDataComponent
    {
        [FormerlySerializedAs("WallData")] public WallData wallData;
    }
}
