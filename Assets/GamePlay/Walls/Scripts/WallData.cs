﻿using Core.Health;
using UnityEngine;
using UnityEngine.Serialization;

namespace GamePlay.Walls.Scripts
{
    [CreateAssetMenu]
    public class WallData : HealthData
    {
        [FormerlySerializedAs("DefensivePower")] public int defensivePower;
        public Sprite destroyedWall;
    }
}
