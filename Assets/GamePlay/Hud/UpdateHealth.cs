﻿using Core.RefVariables;
using TMPro;
using UnityEngine;

namespace GamePlay.Hud
{
    public class UpdateHealth : MonoBehaviour
    {
        [Header("Health")]
        public IntVariable health;
        public GameObject healthObject;

        private TextMeshProUGUI _healthText;
    
        private void Awake()
        {
            _healthText = healthObject.GetComponent<TextMeshProUGUI>();
        }

        public void ChangeHealth()
        {
            if (_healthText == null) return;

            _healthText.text = "Health: " + health.value;
        }
    }
}