﻿using System;
using Core.RefVariables;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace GamePlay.Hud
{
    public class UpdateSpawnInfo : MonoBehaviour
    {
        [Header("SpawnInfo")]
        public SpawnInfoVariable spawnInfo;
        public GameObject spawnInfoTextObject;
        public GameObject spawnInfoImageObject;
        
        private TextMeshProUGUI _spawnInfoText;
        private Image _spawnInfoImage;

        private void Awake()
        {
            _spawnInfoText = spawnInfoTextObject.GetComponent<TextMeshProUGUI>();
            _spawnInfoImage = spawnInfoImageObject.GetComponent<Image>();
        }
        
        public void ChangeSpawnInfo()
        {
            if (spawnInfo == null) return;
            
            if(_spawnInfoImage != null)
                _spawnInfoImage.sprite = spawnInfo.image;
            
            if (_spawnInfoText != null)
                _spawnInfoText.text = spawnInfo.label;
        }
    }
}
