﻿using System;
using System.Linq;
using Core.Health;
using Core.RefVariables;
using UnityEngine;

namespace GamePlay.Upgrades.Scripts
{
    public class ShovelUpgrade : Upgrade
    {
        [Header("Tracking")] public RelatedGameObjectsVariable bunker;

        protected override void Apply(GameObject go)
        {
            if (bunker == null) return;

            foreach (var healthComponent in bunker.relatedGameObjects.Select(brickWall => brickWall.GetComponent<HealthComponent>()))
            {
                healthComponent.ModifyHealth(healthComponent.healthDataComponent.healthData.health - healthComponent.currentHealth, false);
            }
        }
    }
}