﻿using System;
using Core.Input;
using UnityEngine;

namespace GamePlay.Upgrades.Scripts
{
    public abstract class Upgrade : MonoBehaviour
    {
        protected abstract void Apply(GameObject go);

        public void OnTriggerEnter2D(Collider2D other)
        {
            if (other.gameObject.GetComponent<InputComponent>() == null) return;

            Apply(other.gameObject);
            
            Destroy(gameObject);
        }
    }
}