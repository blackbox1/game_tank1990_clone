﻿using System;
using System.Linq;
using Core.Health;
using Core.RefVariables;
using UnityEngine;

namespace GamePlay.Upgrades.Scripts
{
    public class GrenadeUpgrade : Upgrade
    {
        public GameObjectsVariable enemies;
        
        protected override void Apply(GameObject go)
        {
            Debug.Log("Enemies active: " + enemies.activeGameObjects.Count);

            for (var index = enemies.activeGameObjects.Count - 1; index >= 0; --index)
            {
                var hc = enemies.activeGameObjects[index].GetComponent<HealthComponent>();
                
                hc.ModifyHealth(-hc.currentHealth, true);
            }

            Debug.Log("Enemies active: " + enemies.activeGameObjects.Count);
        }
    }
}
