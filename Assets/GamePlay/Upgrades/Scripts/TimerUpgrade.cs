﻿using Core.Health;
using Core.RefVariables;
using GamePlay.Tanks.Scripts;
using UnityEngine;

namespace GamePlay.Upgrades.Scripts
{
    public class TimerUpgrade : Upgrade
    {
        [Header("Timer")] 
        public float timerDuration;
        public Color timerColor;
        
        [Header("Enemies")] 
        public GameObjectsVariable enemies;
        
        protected override void Apply(GameObject go)
        {
            foreach (var enemy in enemies.activeGameObjects)
            {
                var mc = enemy.GetComponent<TankMovementComponent>();
                var sc = enemy.GetComponent<TankShootComponent>();
                var hc = enemy.GetComponent<HealthComponent>();

                mc.Deactivate(timerDuration);
                sc.Deactivate(timerDuration);
                
                hc.ChangeColorForDuration(timerDuration, timerColor);
            } 
        }
    }
}