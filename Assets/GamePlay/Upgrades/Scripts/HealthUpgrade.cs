﻿using System;
using Core.GameEvent;
using Core.Health;
using Core.RefVariables;
using UnityEngine;

namespace GamePlay.Upgrades.Scripts
{
    public class HealthUpgrade : Upgrade
    {
        public IntVariable health;

        public GameEvent healthUpdateEvent;
        
        protected override void Apply(GameObject go)
        {
            health.value++;
            healthUpdateEvent.Raise();
        }
    }
}