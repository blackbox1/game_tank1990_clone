﻿using GamePlay.Tanks.Scripts;
using Unity.Mathematics;
using UnityEngine;

namespace GamePlay.Upgrades.Scripts
{
    public class StarUpgrade : Upgrade
    {
        private const int MaxShellLevel = 4;
        private const int MinShellLevel = 1;

        protected override void Apply(GameObject go)
        {
            if (go == null) return;
            
            var tdc = go.GetComponent<TankDataComponent>();

            if (tdc == null) return;

            tdc.CreateInstancedTankData();

            var td = tdc.currentTankData;

            td.shellLevel++;
            td.shellLevel = math.clamp(td.shellLevel, MinShellLevel, MaxShellLevel);
            
            Debug.Log("power level: " + td.shellLevel);

            //TODO: RESET TO DEFAULT ON DEATH
            switch (td.shellLevel)
            {
                case 2:
                    td.shellSpeed = 3;
                    break;
                case 3:
                    td.activeShells = 2;
                    break;
                case 4:
                    td.shellPower = 2;
                    break;
            }
        }
    }
}