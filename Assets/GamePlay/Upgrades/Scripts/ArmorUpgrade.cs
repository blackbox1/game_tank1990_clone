﻿using System;
using Core.Health;
using UnityEngine;

namespace GamePlay.Upgrades.Scripts
{
    public class ArmorUpgrade : Upgrade
    {
        [Header("Shield")] 
        public Color shieldColor;
        public float shieldDurationInSeconds;
        
        protected override void Apply(GameObject go)
        {
            if (go == null) return;
            
            var hc = go.GetComponent<HealthComponent>();

            if (hc == null) return;
            
            hc.ApplyShield(shieldDurationInSeconds);
            hc.ChangeColorForDuration(shieldDurationInSeconds, shieldColor);
        }
    }
}