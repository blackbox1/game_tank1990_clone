﻿using UnityEngine;

namespace Effects.Scripts
{
    public class TrailObject : MonoBehaviour
    {
        public SpriteRenderer renderer;
        public Color startColor, endColor;

        private bool _inUse;
        private Vector2 _position;
        private float _displayTime;
        private float _timeDisplayed;
        private TrailEffect _trailEffect;

        private void Awake()
        {
            renderer.enabled = false;
        }
        
        private void Update ()
        {
            if (!_inUse) return;
            
            _timeDisplayed += Time.deltaTime;

            renderer.color = Color.Lerp (startColor, endColor, _timeDisplayed / _displayTime);

            if (!(_timeDisplayed >= _displayTime)) return;
            
            _trailEffect.RemoveTrailObject (gameObject);
            _inUse = false;
            renderer.enabled = false;
        }

        public void Initiate (float displayTime, Sprite trail, Vector2 position, Quaternion rotation, TrailEffect trailEffect)
        {
            _displayTime = displayTime;
            transform.position = position;
            transform.rotation = rotation;
            _timeDisplayed = 0;
            _trailEffect = trailEffect;
            _inUse = true;
            renderer.enabled = true;
            
            if(trail != null)
                renderer.sprite = trail;
        }
    }
}
