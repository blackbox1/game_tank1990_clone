﻿using System.Collections.Generic;
using UnityEngine;

namespace Effects.Scripts
{
    public class TrailEffect : MonoBehaviour
    {
        [Header("Trail Settings")]
        public int trailSegments;
        public float trailTime;
        public GameObject trailObject;
        
        [Header("Trails")] 
        public bool useForwardDirection;
        public Sprite horizontalTrails;
        public Sprite verticalTrails;
        
        [HideInInspector] public Vector3 forwardDirection;

        private float _spawnInterval;
        private float _spawnTimer;
        private bool _enabled;

        private List<GameObject> _trailObjectsInUse;
        private Queue<GameObject> _trailObjectsNotInUse;
        
        private void Start ()
        {
            _spawnInterval = trailTime / trailSegments;
            _trailObjectsInUse = new List<GameObject> ();
            _trailObjectsNotInUse = new Queue<GameObject> ();

            for (var i = 0; i < trailSegments; i++)
            {
                var trail = Instantiate(trailObject, transform.position, Quaternion.identity);
                _trailObjectsNotInUse.Enqueue (trail);		
            }
            
            SetEnabled(false);
        }
        
        private void Update ()
        {
            if (!_enabled) return;
            
            _spawnTimer += Time.deltaTime;

            if (!(_spawnTimer >= _spawnInterval)) return;

            var trail = _trailObjectsNotInUse.Dequeue ();
            
            if (trail == null) return;
            
            var trailObj = trail.GetComponent<TrailObject> ();

            if (useForwardDirection)
            {
                var spriteToUse = Mathf.Abs(forwardDirection.x) > 0.01f ? horizontalTrails : verticalTrails;
                trailObj.Initiate(trailTime, spriteToUse, transform.position, Quaternion.identity, this);
            }
            else
            {
                var angle = Mathf.Atan2(forwardDirection.y, forwardDirection.x) * Mathf.Rad2Deg;
                trailObj.Initiate(trailTime, null, transform.position, Quaternion.AngleAxis(angle, Vector3.forward),this);
            }
            
            _trailObjectsInUse.Add (trail);

            _spawnTimer = 0;
        }

        public void RemoveTrailObject (GameObject obj)
        {
            _trailObjectsInUse.Remove (obj);
            _trailObjectsNotInUse.Enqueue (obj);
        }

        public void SetEnabled (bool enable)
        {
            if (_enabled == enable) return;
            
            _enabled = enable;

            if (enable)
            {
                _spawnTimer = _spawnInterval;
            }
        }
    }
}
